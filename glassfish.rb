require 'fileutils'
require 'tempfile'  # Dir.tmpdir

set :glassfish_location, "/usr/local/glassfish"
set :glassfish_domain, "middleware"
set :glassfish_application_name, "middleware"
set :glassfish_extra, "--user admin --passwordfile /home/ubuntu/pass.txt  --port 4848"
set :war, "middleware-0.0.1-SNAPSHOT.war"


#Build locally the war file and deploys to the glassfish servers
namespace :glassfish do
    task :deploy do
        check #Verify paths
        checkout #Checkout code only locally
        build #Guess what
        glassfish.remote_deploy
        cleanup 
    end

    def build
        run_locally("cd #{current_path} && mvn -e clean install -Dmaven.test.skip -Pdeployment  -Djdbc.properties.dir=src/main/resources/META-INF/ -Dmongo.properties.dir=src/main/resources/META-INF/ -Delasticsearch.properties.dir=src/main/resources/META-INF/ ")
    end

    #The versions are kept only locally
    task :rollback do
        if previous_release
            run_locally "rm #{current_path}; ln -s #{previous_release} #{current_path}"
            run_locally "rm -rf #{current_release}"
            glassfish.remote_deploy
        else
            abort "could not rollback the code because there is no prior release"
        end
    end

    #Using the default webistrano behavior and dsl
    task :remote_deploy do
        upload(File.join(current_path,'target',war),"/root/#{war}")
        glassfish.undeploy
        glassfish.restart
        glassfish.redeploy
    end

    task :start do
        run "#{glassfish_location}/bin/asadmin #{glassfish_extra} start-domain #{glassfish_domain}"
    end

    task :restart do
        run "#{glassfish_location}/bin/asadmin #{glassfish_extra} restart-domain #{glassfish_domain}"
    end

    task :undeploy do
        run "#{glassfish_location}/bin/asadmin #{glassfish_extra} undeploy #{glassfish_application_name} || true"
    end

    task :redeploy do
        run "#{glassfish_location}/bin/asadmin #{glassfish_extra} deploy --name #{glassfish_application_name} /root/#{war}"
    end

    # Rewrite to return the output of the comment into the logger
    def run_locally(cmd)
        logger.trace "executing locally: #{cmd.inspect}" if logger
        out = IO.popen(cmd)
        logger.trace out.readlines
    end

    # References
    # https://github.com/capistrano/capistrano/blob/master/lib/capistrano/recipes/deploy.rb
    # https://github.com/capistrano/capistrano/blob/master/lib/capistrano/recipes/deploy/strategy/copy.rb
    # https://github.com/capistrano/capistrano/blob/master/lib/capistrano/recipes/deploy/scm/git.rb

    def check
        if !File.directory?(checkout_path)
            logger.trace "Creating checkout directory #{checkout_path}"
            FileUtils.mkdir_p(checkout_path)
        end
        if !File.writable?(checkout_path) 
            logger.trace "You do not have permissions to write to `#{checkout_path}'."
            throw "You do not have permissions to write to `#{checkout_path}'."
        end
        if !File.directory?(release_dir)
            logger.trace "Creating release directory #{release_dir}"
            FileUtils.mkdir_p(release_dir)
            if !File.writable?(release_dir) 
                logger.trace "You do not have permissions to write to `#{release_dir}'."
                throw "You do not have permissions to write to `#{release_dir}'."
            end
        end
    end

    def checkout
        run_locally(source.checkout(real_revision, release_dir))
        run_locally("rm -f #{current_path}; ln -s #{release_dir} #{current_path}; true")
    end

    #Rewriting the cleanup routine to deal with multiple versions LOCALLY
    def cleanup
        count = fetch(:keep_releases, 5).to_i
        if count >= releases.length
            logger.important "no old releases to clean up"
        else
            logger.info "keeping #{count} of #{releases.length} deployed releases"

            directories = (releases - releases.last(count)).map { |release| File.join(releases_path, release) }.join(" ")
            run_locally "rm -rf #{directories}"
        end
    end

    def releases_path 
        @releases_path ||= File.join(checkout_path,version_dir)
    end

    def checkout_path
        @checkout_path ||= checkout_dir ? checkout_dir : Dir.tmpdir
    end

    def release_dir
        @release_dir ||= File.join(releases_path,release_name)
    end

    def current_path
        @current_path ||= File.join(checkout_path, current_dir)
    end

    def releases
        releases = %x[ls -xt #{releases_path}]
        releases.split.reverse
    end

    def previous_release
        releases.length > 1 ? File.join(releases_path, releases[-2]) : nil        
    end

    def current_release
        File.join(releases_path, releases.last)
    end
end

#
# Disable all the default tasks that
# either don't apply, or I haven't made work.
# Source: https://github.com/andynu/capistrano-recipes/blob/master/capfile_tomcat
namespace :deploy do
    task :default do
      glassfish.deploy
    end
    namespace :rollback do
        task :default do
          glassfish.rollback
        end        
    end
end
